# Get the AMI for our bastion
data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_filter]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }

  owners = [var.ami_owner]
}

data "aws_subnet" "subnet" {
  id = var.subnet_ids[0]
}

data "template_file" "gitlab_config" {
  template = file("${path.module}/templates/user-data.sh")
  vars = {
    log_group            = aws_cloudwatch_log_group.log_group.name
    token                = var.gitlab_token
    config_secret_id     = var.gitlab_config_secret_id
    cache_bucket         = aws_s3_bucket.cache.bucket
    region               = var.aws_region
    ami                  = data.aws_ami.ami.image_id
    vpc_id               = var.vpc_id
    subnet_id            = data.aws_subnet.subnet.id
    subnet_az            = substr(data.aws_subnet.subnet.availability_zone, -1, 1)
    instance_type        = var.instance_type
    security_group       = aws_security_group.security-group.name
    runner_version       = var.gitlab_runner_version
    runner_name          = var.runner_name
    runner_tags          = var.runner_tags
    gitlab_url           = var.gitlab_url
    tags                 = "%{for tag, val in local.service_tags~} %{if tag != "Name"}${tag},${val},%{endif} %{endfor~}"
    iam_instance_profile = var.machine_iam_instance_profile
    idle_nodes           = var.machine_idle_nodes
    idle_time            = var.machine_idle_time
    max_builds           = var.machine_max_builds
    off_peak_timezone    = var.machine_off_peak_timezone
    off_peak_idle_nodes  = var.machine_off_peak_idle_nodes
    off_peak_idle_time   = var.machine_off_peak_idle_time
  }
}

resource "aws_launch_template" "template" {
  name_prefix   = "${local.service_tags.Name}-"
  image_id      = data.aws_ami.ami.image_id
  instance_type = "t3.micro"
  key_name      = var.ssh_key_name
  user_data     = base64encode(data.template_file.gitlab_config.rendered)

  iam_instance_profile {
    arn = aws_iam_instance_profile.profile.arn
  }

  network_interfaces {
    security_groups       = [aws_security_group.security-group.id]
    delete_on_termination = true
  }

  tag_specifications {
    resource_type = "instance"

    tags = local.service_tags
  }
}

resource "aws_autoscaling_group" "asg" {
  name                 = local.service_tags.Name
  max_size             = 1
  min_size             = 1
  desired_capacity     = 1
  force_delete         = true
  vpc_zone_identifier  = var.subnet_ids
  termination_policies = ["OldestInstance"]

  launch_template {
    id      = aws_launch_template.template.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = local.service_tags.Name
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Environment"
    value               = local.service_tags.Environment
    propagate_at_launch = "true"
  }

  tag {
    key                 = "ResponsibleParty"
    value               = local.service_tags.ResponsibleParty
    propagate_at_launch = "true"
  }

  tag {
    key                 = "DataRisk"
    value               = local.service_tags.DataRisk
    propagate_at_launch = "true"
  }

  tag {
    key                 = "ComplianceRisk"
    value               = local.service_tags.ComplianceRisk
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Comments"
    value               = local.service_tags.Comments
    propagate_at_launch = "true"
  }

  tag {
    key                 = "VCS"
    value               = local.service_tags.VCS
    propagate_at_launch = "true"
  }
}

