# gitlab-runner

## Purpose

This module provides an AWS Docker-Machine based gitlab-runner for your CI/CD work.

## Description

Provisions resources necessary to run gitlab-runner in AWS with autoscaling of
EC2 instances using Docker Machine. See [here](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/).

![architectural diagram](images/gitlab-runner.png)

EC2 instance gitlab-runner-bastion uses Docker Machine to spin up additional
EC2 instances as needed to support your build needs.

## Usage Instructions

Copy and paste into your Terraform configuration, insert or update the
variables, and run `terraform init`:

```
module "gitlab-runner" {
  source                   = "git@code.vt.edu:devcom/terraform-modules/aws-gitlab-runner.git"
  environment              = "all"
  gitlab_runner_cache_name = "vt-foo-gitlab-runner-cache"
  gitlab_token             = "my-ci-cd-token-from-gitlab"
  responsible_party        = "sally"
  runner_name              = "foo-aws-gitlab-runner"
  ssh_key_name             = "sally"
  subnet_ids               = ["subnet-123456", "subnet-67890"]
  vcs                      = "git@code.vt.edu:path/to/my/repo"
  vpc_id                   = "vpc-123456"
  ...
}
```

## Preconditions and Assumptions

The following need to exist in AWS already:

* An SSH key (for logging in to EC2 instances)
* VPC + Subnet

You also need to [generate a runner token in Gitlab](https://docs.gitlab.com/ee/ci/runners/).

## Dependencies

* [vt-globals](https://code.vt.edu/devcom/terraform-modules/vt-globals)

## Inputs

### Required Inputs
These variables are required.

| Name | Type | Description |
| ---- | ---- | ----------- |
| **gitlab_runner_cache_name** | `[string]` | S3 bucket name to be used for the gitlab runner cache |
| **responsible_party** | `[string]` | person (pid) who is primarily responsible for the configuration and maintenance of this resource |
| **ssh_key_name** | `[string]` | Name of the SSH key you want to use to access the bastion
| **subnet_ids** | `[string]` | List of subnet ids in which to create resources
| **vcs** | `[string]` | A link to the repo in a version control system (usually Git) that manages this resource.
| **vpc_id** | `[string]` | The ID of the VPC we should use for EC2 instances

### Optional Inputs
These variables have default values and don't have to be set to use this module.
You may set these variables to override their default values.


| Name | Type | Description | Default |
| ---- | ---- | ----------- | ------- |
| **ami_filter** | `[string]` | name filter for EC2 AMI | `ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-????????`
| **ami_owner** | `[string]` | Onwer of the EC2 AMI | `099720109477` (Canonical)
| **ami_user** | `[string]` | default user for the EC2 AMI | `ubuntu`
| **aws_region** | `[string]` | AWS region | `us-east-1`
| **compliance_risk** | `[string]` | should be `none`, `ferpa` or `pii` | `none`
| **data_risk** | `[string]` | should be `low`, `medium` or `high` based on data-risk classifications defined [here](http://it.vt.edu/content/dam/it_vt_edu/policies/Virginia-Tech-Risk-Classifications.pdf) | `low`
| **documentation** | `[string]` | link to documentation and/or history file | `none`
| **environment** | `[string]` | e.g. `development`, `test`, or `production` | `build`
| **gitlab_config_secret_id** | `[string]` | AWS Secrets Manager secret id for secret containing your complete gitlab runner config | `none` |
| **gitlab_token** | `[string]` | Gitlab _registration_ token for your group or project (this option is **deprecated** in favour of gitlab_config_secret_id) | `none` |
| **gitlab_url** | `[string]` | URL to the Gitlab instance the runner will serve | `https://code.vt.edu/`
| **instance_type** | `[string]` | The EC2 instance type to use for the docker machines | `m4.large`
| **log_retention** | `[string]` | Number of days to keep logs from Docker Machines created by gitlab-runner | 14
| **machine_iam_instance_profile** | `[string]` | Name of an AWS instance profile to assign to new worker instances | `""`
| **machine_idle_nodes** | `[string]` | Maximum idle machines | 0
| **machine_idle_time** | `[string]` | Minimum time after node can be destroyed | 300
| **machine_max_builds** | `[string]` | Maximum number of builds processed by machine | 10
| **machine_off_peak_idle_nodes** | `[string]` | Maximum idle machines when the scheduler is in the OffPeak mode | 0
| **machine_off_peak_idle_time** | `[string]` | Minimum time after machine can be destroyed when the scheduler is in the OffPeak mode | 120
| **machine_off_peak_timezone** | `[string]` | Timezone for the OffPeak periods | `America/New_York`
| **responsible_party2** | `[string]` | Backup for responsible_party | `none`
| **runner_name** | `[string]` | The runner name as seen in Gitlab | `gitlab-runner`
| **runner_tags** | `[string]` | Tags to apply to the runner, comma-separated | `""`
| **service_name** | `[string]` | The high level service this resource is primarily supporting | `build`
| **use_public_ip_for_bastion** | `[boolean]` | Whether or not to allocate a public ip for the bastion instance | `false`

**Notes**

1. One of `gitlab_config_secret_id` or `gitlab_token` must be provided
2. The secret referenced by `gitlab_config_secret_id` should be a valid gitlab-runner config.toml file (not JSON).
3. offpeak period is defined as the union of the following cron expressions:

```
* * 0-6,18-23 * * mon-fri *
* * * * * sat,sun *
```

## Outputs
This module provides no outputs.

## Versions

| Version | Major changes |
| ------- | ------------- |
| 1       | Created module |
| 1.1     | Cleanup key pairs, terminate EC2 instances and use same AMI for docker machines |
| 1.2     | Allow passing Gitlab Runner config as an AWS secret |

## TODO

- [ ] Switch to using AWS Spot instances 
